ARG VERSION
FROM nvidia/cudagl:$VERSION

RUN mkdir -p /usr/local/nvidia/lib64 && \
    mv /usr/local/lib/x86_64-linux-gnu/* /usr/local/nvidia/lib64

VOLUME /usr/local/nvidia

CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
