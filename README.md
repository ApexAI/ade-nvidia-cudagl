## ADE nvidia-cudagl

This project serves as a thin wrapper to generate an `ade` volume to `nvidia-docker2` with
an Ubuntu 16.04 (Xenial) base image.

See https://gitlab.com/ApexAI/ade-cli for more information.

### Background

`nvidia-docker1` provided a volume with the correct OpenGL libraries to run
GUI apps inside an `ubuntu:xenial`-based Docker image. This volume would provide the correct
libraries to successfully run the applications.

`nvidia-docker2` does not provide this volume by default, so there is a library mismatch
between the libraries in the `ubuntu:xenial`-based Docker image and the required NVidia
libraries. This mismatch does not occur with `ubuntu:bionic`-based Docker images because
`ubuntu:bionic` happens to have the correct libraries.

This project is meant to imitate the volume provided by `nvidia-docker1` in order to support
`nvidia-docker2` with `ade-cli`.
